// ♓🌟 Piscēs ∷ collection.js
// ====================================================================
//
// Copyright © 2020–2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { call, makeCallable } from "./function.js";
import {
  floor,
  isIntegralNumber,
  isNan,
  max,
  MAXIMUM_SAFE_INTEGRAL_NUMBER,
  min,
} from "./numeric.js";
import { sameValue, type } from "./value.js";

export const {
  /** Returns an array of the provided values. */
  of: array,

  /** Returns whether the provided value is an array. */
  isArray,

  /** Returns an array created from the provided arraylike. */
  from: toArray,
} = Array;

/**
 * Returns -0 if the provided argument is "-0"; returns a number
 * representing the index if the provided argument is a canonical
 * numeric index string; otherwise, returns undefined.
 *
 * There is no clamping of the numeric index, but note that numbers
 * above 2^53 − 1 are not safe nor valid integer indices.
 */
export const canonicalNumericIndexString = ($) => {
  if (typeof $ !== "string") {
    return undefined;
  } else if ($ === "-0") {
    return -0;
  } else {
    const n = +$;
    return $ === `${n}` ? n : undefined;
  }
};

/**
 * Returns the result of catenating the provided arraylikes, returning
 * a new collection according to the algorithm of Array::concat.
 */
export const catenate = makeCallable(Array.prototype.concat);

/**
 * Copies the items in the provided object to a new location according
 * to the algorithm of Array::copyWithin.
 */
export const copyWithin = makeCallable(Array.prototype.copyWithin);

/**
 * Fills the provided object with the provided value using the
 * algorithm of Array::fill.
 */
export const fill = makeCallable(Array.prototype.fill);

/**
 * Returns the result of filtering the provided object with the
 * provided callback, using the algorithm of Array::filter.
 */
export const filter = makeCallable(Array.prototype.filter);

/**
 * Returns the first index in the provided object whose value satisfies
 * the provided callback using the algorithm of Array::findIndex.
 */
export const findIndex = makeCallable(Array.prototype.findIndex);

/**
 * Returns the first indexed entry in the provided object whose value
 * satisfies the provided callback.
 *
 * If a third argument is supplied, it will be used as the this value
 * of the callback.
 */
export const findIndexedEntry = (
  $,
  callback,
  thisArg = undefined,
) => {
  let result = undefined;
  findItem($, (kValue, k, O) => {
    if (call(callback, thisArg, [kValue, k, O])) {
      // The callback succeeded.
      result = [k, kValue];
      return true;
    } else {
      // The callback failed.
      return false;
    }
  });
  return result;
};

/**
 * Returns the first indexed value in the provided object which
 * satisfies the provided callback, using the algorithm of Array::find.
 */
export const findItem = makeCallable(Array.prototype.find);

/**
 * Returns the result of flatmapping the provided value with the
 * provided callback using the algorithm of Array::flatMap.
 */
export const flatmap = makeCallable(Array.prototype.flatMap);

/**
 * Returns the result of flattening the provided object using the
 * algorithm of Array::flat.
 */
export const flatten = makeCallable(Array.prototype.flat);

/**
 * Returns the first index of the provided object with a value
 * equivalent to the provided value according to the algorithm of
 * Array::indexOf.
 */
export const getFirstIndex = makeCallable(Array.prototype.indexOf);

/**
 * Returns the item on the provided object at the provided index using
 * the algorithm of Array::at.
 */
export const getItem = makeCallable(Array.prototype.at);

/**
 * Returns the last index of the provided object with a value
 * equivalent to the provided value according to the algorithm of
 * Array::lastIndexOf.
 */
export const getLastIndex = makeCallable(Array.prototype.lastIndexOf);

/**
 * Returns whether every indexed value in the provided object satisfies
 * the provided function, using the algorithm of Array::every.
 */
export const hasEvery = makeCallable(Array.prototype.every);

/**
 * Returns whether the provided object has an indexed value which
 * satisfies the provided function, using the algorithm of Array::some.
 */
export const hasSome = makeCallable(Array.prototype.some);

/**
 * Returns whether the provided object has an indexed value equivalent
 * to the provided value according to the algorithm of Array::includes.
 *
 * > ☡ This algorithm treats missing values as `undefined` rather than
 * > skipping them.
 */
export const includes = makeCallable(Array.prototype.includes);

/**
 * Returns an iterator over the indexed entries in the provided value
 * according to the algorithm of Array::entries.
 */
export const indexedEntries = makeCallable(Array.prototype.entries);

/**
 * Returns an iterator over the indices in the provided value according
 * to the algorithm of Array::keys.
 */
export const indices = makeCallable(Array.prototype.keys);

/** Returns whether the provided value is an array index string. */
export const isArrayIndexString = ($) => {
  const value = canonicalNumericIndexString($);
  if (value !== undefined) {
    // The provided value is a canonical numeric index string.
    return sameValue(value, 0) || value > 0 && value < -1 >>> 0 &&
        value === toLength(value);
  } else {
    // The provided value is not a canonical numeric index string.
    return false;
  }
};

/** Returns whether the provided value is arraylike. */
export const isArraylikeObject = ($) => {
  if (type($) !== "object") {
    return false;
  } else {
    try {
      lengthOfArraylike($); // throws if not arraylike
      return true;
    } catch {
      return false;
    }
  }
};

/**
 * Returns whether the provided object is a collection.
 *
 * The definition of “collection” used by Piscēs is similar to
 * Ecmascript’s definition of an arraylike object, but it differs in
 * a few ways :—
 *
 * - It requires the provided value to be a proper object.
 *
 * - It requires the `length` property to be an integer index.
 *
 * - It requires the object to be concat‐spreadable, meaning it must
 *   either be an array or have `[Symbol.isConcatSpreadable]` be true.
 */
export const isCollection = ($) => {
  if (!(type($) === "object" && "length" in $)) {
    // The provided value is not an object or does not have a `length`.
    return false;
  } else {
    try {
      toIndex($.length); // will throw if `length` is not an index
      return isConcatSpreadable($);
    } catch {
      return false;
    }
  }
};

/**
 * Returns whether the provided value is spreadable during array
 * concatenation.
 *
 * This is also used to determine which things should be treated as
 * collections.
 */
export const isConcatSpreadable = ($) => {
  if (type($) !== "object") {
    // The provided value is not an object.
    return false;
  } else {
    // The provided value is an object.
    const spreadable = $[Symbol.isConcatSpreadable];
    return spreadable !== undefined ? !!spreadable : isArray($);
  }
};

/** Returns whether the provided value is an integer index string. */
export const isIntegerIndexString = ($) => {
  const value = canonicalNumericIndexString($);
  if (value !== undefined && isIntegralNumber(value)) {
    // The provided value is a canonical numeric index string.
    return sameValue(value, 0) ||
      value > 0 && value <= MAXIMUM_SAFE_INTEGRAL_NUMBER &&
        value === toLength(value);
  } else {
    // The provided value is not a canonical numeric index string.
    return false;
  }
};

/**
 * Returns an iterator over the items in the provided value according
 * to the algorithm of Array::values.
 */
export const items = makeCallable(Array.prototype.values);

/**
 * Returns the length of the provided arraylike object.
 *
 * Will throw if the provided object is not arraylike.
 *
 * This can produce larger lengths than can actually be stored in
 * arrays, because no such restrictions exist on arraylike methods.
 */
export const lengthOfArraylike = ({ length }) => toLength(length);

/**
 * Returns the result of mapping the provided value with the provided
 * callback using the algorithm of Array::map.
 */
export const map = makeCallable(Array.prototype.map);

/** Pops from the provided value using the algorithm of Array::pop. */
export const pop = makeCallable(Array.prototype.pop);

/**
 * Pushes onto the provided value using the algorithm of Array::push.
 */
export const push = makeCallable(Array.prototype.push);

/**
 * Returns the result of reducing the provided value with the provided
 * callback, using the algorithm of Array::reduce.
 */
export const reduce = makeCallable(Array.prototype.reduce);

/**
 * Reverses the provided value using the algorithm of Array::reverse.
 */
export const reverse = makeCallable(Array.prototype.reverse);

/** Shifts the provided value using the algorithm of Array::shift. */
export const shift = makeCallable(Array.prototype.shift);

/**
 * Returns a slice of the provided value using the algorithm of
 * Array::slice.
 */
export const slice = makeCallable(Array.prototype.slice);

/**
 * Sorts the provided value in‐place using the algorithm of
 * Array::sort.
 */
export const sort = makeCallable(Array.prototype.sort);

/**
 * Splices into and out of the provided value using the algorithm of
 * Array::splice.
 */
export const splice = makeCallable(Array.prototype.splice);

/**
 * Returns the result of converting the provided value to an array
 * index, or throws an error if it is out of range.
 */
export const toIndex = ($) => {
  const integer = floor($);
  if (isNan(integer) || integer == 0) {
    // The value is zero·like.
    return 0;
  } else {
    // The value is not zero·like.
    const clamped = toLength(integer);
    if (clamped !== integer) {
      // Clamping the value changes it.
      throw new RangeError(`Piscēs: Index out of range: ${$}.`);
    } else {
      // The value is within appropriate bounds.
      return integer;
    }
  }
};

/** Returns the result of converting the provided value to a length. */
export const toLength = ($) => {
  const len = floor($);
  return isNan(len) || len == 0
    ? 0
    : max(min(len, MAXIMUM_SAFE_INTEGRAL_NUMBER), 0);
};

/**
 * Unshifts the provided value using the algorithm of Array::unshift.
 */
export const unshift = makeCallable(Array.prototype.unshift);
