// ♓🌟 Piscēs ∷ numeric.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  assertStrictEquals,
  assertThrows,
  describe,
  it,
} from "./dev-deps.js";
import {
  abs,
  atan2,
  clz32,
  max,
  min,
  NEGATIVE_ZERO,
  POSITIVE_ZERO,
  sgn,
  toBigInt,
  toExponentialNotation,
  toFixedDecimalNotation,
  toFloat32,
  toIntegralNumber,
  toIntegralNumberOrInfinity,
  toIntN,
  toNumber,
  toNumeric,
  toUintN,
} from "./numeric.js";

describe("NEGATIVE_ZERO", () => {
  it("[[Get]] is negative zero", () => {
    assertStrictEquals(NEGATIVE_ZERO, -0);
  });
});

describe("POSITIVE_ZERO", () => {
  it("[[Get]] is positive zero", () => {
    assertStrictEquals(POSITIVE_ZERO, 0);
  });
});

describe("abs", () => {
  it("[[Call]] returns the absolute value", () => {
    assertStrictEquals(abs(-1), 1);
  });

  it("[[Call]] works with big·ints", () => {
    const bn = BigInt(Number.MAX_SAFE_INTEGER) + 2n;
    assertStrictEquals(abs(-bn), bn);
  });
});

describe("atan2", () => {
  it("[[Call]] returns the atan2", () => {
    assertStrictEquals(atan2(6, 9), Math.atan2(6, 9));
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(atan2(6n, 9n), Math.atan2(6, 9));
  });
});

describe("clz32", () => {
  it("[[Call]] returns the clz32", () => {
    assertStrictEquals(clz32(1 << 28), 3);
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(clz32(1n << 28n), 3);
  });
});

describe("max", () => {
  it("[[Call]] returns the largest number", () => {
    assertStrictEquals(max(1, -6, 92, -Infinity, 0), 92);
  });

  it("[[Call]] returns the largest big·int", () => {
    assertStrictEquals(max(1n, -6n, 92n, 0n), 92n);
  });

  it("[[Call]] returns nan if any argument is nan", () => {
    assertStrictEquals(max(0, NaN, 1), NaN);
  });

  it("[[Call]] returns -Infinity when called with no arguments", () => {
    assertStrictEquals(max(), -Infinity);
  });

  it("[[Call]] throws if both big·int and number arguments are provided", () => {
    assertThrows(() => max(-Infinity, 0n));
  });
});

describe("min", () => {
  it("[[Call]] returns the largest number", () => {
    assertStrictEquals(min(1, -6, 92, Infinity, 0), -6);
  });

  it("[[Call]] returns the largest big·int", () => {
    assertStrictEquals(min(1n, -6n, 92n, 0n), -6n);
  });

  it("[[Call]] returns nan if any argument is nan", () => {
    assertStrictEquals(min(0, NaN, 1), NaN);
  });

  it("[[Call]] returns Infinity when called with no arguments", () => {
    assertStrictEquals(min(), Infinity);
  });

  it("[[Call]] throws if both big·int and number arguments are provided", () => {
    assertThrows(() => min(Infinity, 0n));
  });
});

describe("sgn", () => {
  it("[[Call]] returns the sign", () => {
    assertStrictEquals(sgn(Infinity), 1);
    assertStrictEquals(sgn(-Infinity), -1);
    assertStrictEquals(sgn(0), 0);
    assertStrictEquals(sgn(-0), -0);
    assertStrictEquals(sgn(NaN), NaN);
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(sgn(0n), 0n);
    assertStrictEquals(sgn(92n), 1n);
    assertStrictEquals(sgn(-92n), -1n);
  });
});

describe("toBigInt", () => {
  it("[[Call]] converts to a big·int", () => {
    assertStrictEquals(toBigInt(2), 2n);
  });
});

describe("toExponentialNotation", () => {
  it("[[Call]] converts to exponential notation", () => {
    assertStrictEquals(toExponentialNotation(231), "2.31e+2");
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(
      toExponentialNotation(9007199254740993n),
      "9.007199254740993e+15",
    );
  });

  it("[[Call]] respects the specified number of fractional digits", () => {
    assertStrictEquals(
      toExponentialNotation(.00000000642, 3),
      "6.420e-9",
    );
    assertStrictEquals(toExponentialNotation(.00691, 1), "6.9e-3");
    assertStrictEquals(toExponentialNotation(.00685, 1), "6.9e-3");
    assertStrictEquals(toExponentialNotation(.004199, 2), "4.20e-3");
    assertStrictEquals(
      toExponentialNotation(6420000000n, 3),
      "6.420e+9",
    );
    assertStrictEquals(toExponentialNotation(6910n, 1), "6.9e+3");
    assertStrictEquals(toExponentialNotation(6850n, 1), "6.9e+3");
    assertStrictEquals(toExponentialNotation(4199n, 2), "4.20e+3");
  });
});

describe("toFixedDecimalNotation", () => {
  it("[[Call]] converts to fixed decimal notation", () => {
    assertStrictEquals(toFixedDecimalNotation(69.4199, 3), "69.420");
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(
      toFixedDecimalNotation(9007199254740993n),
      "9007199254740993",
    );
    assertStrictEquals(
      toFixedDecimalNotation(9007199254740993n, 2),
      "9007199254740993.00",
    );
  });
});

describe("toFloat32", () => {
  it("[[Call]] returns the 32‐bit floating‐point representation", () => {
    assertStrictEquals(
      toFloat32(562949953421313),
      Math.fround(562949953421313),
    );
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(
      toFloat32(562949953421313n),
      Math.fround(562949953421313),
    );
  });
});

describe("toIntN", () => {
  it("[[Call]] converts to an int·n", () => {
    assertStrictEquals(toIntN(2, 7n), -1n);
  });

  it("[[Call]] works with numbers", () => {
    assertStrictEquals(toIntN(2, 7), -1);
  });

  it("[[Call]] works with non‐integers", () => {
    assertStrictEquals(toIntN(2, 7.21), -1);
    assertStrictEquals(toIntN(2, Infinity), 0);
  });
});

describe("toIntegralNumber", () => {
  it("[[Call]] converts nan to zero", () => {
    assertStrictEquals(toIntegralNumber(NaN), 0);
  });

  it("[[Call]] converts negative zero to positive zero", () => {
    assertStrictEquals(toIntegralNumber(-0), 0);
  });

  it("[[Call]] drops the fractional part of negative numbers", () => {
    assertStrictEquals(toIntegralNumber(-1.79), -1);
  });

  it("[[Call]] returns zero for infinity", () => {
    assertStrictEquals(toIntegralNumber(Infinity), 0);
  });

  it("[[Call]] returns zero for negative infinity", () => {
    assertStrictEquals(toIntegralNumber(-Infinity), 0);
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(toIntegralNumber(2n), 2);
  });
});

describe("toIntegralNumberOrInfinity", () => {
  it("[[Call]] converts nan to zero", () => {
    assertStrictEquals(toIntegralNumberOrInfinity(NaN), 0);
  });

  it("[[Call]] converts negative zero to positive zero", () => {
    assertStrictEquals(toIntegralNumberOrInfinity(-0), 0);
  });

  it("[[Call]] drops the fractional part of negative numbers", () => {
    assertStrictEquals(toIntegralNumberOrInfinity(-1.79), -1);
  });

  it("[[Call]] returns infinity for infinity", () => {
    assertStrictEquals(toIntegralNumberOrInfinity(Infinity), Infinity);
  });

  it("[[Call]] returns negative infinity for negative infinity", () => {
    assertStrictEquals(
      toIntegralNumberOrInfinity(-Infinity),
      -Infinity,
    );
  });

  it("[[Call]] works with big·ints", () => {
    assertStrictEquals(toIntegralNumberOrInfinity(2n), 2);
  });
});

describe("toNumber", () => {
  it("[[Call]] converts to a number", () => {
    assertStrictEquals(toNumber(2n), 2);
  });
});

describe("toNumeric", () => {
  it("[[Call]] returns a big·int argument", () => {
    assertStrictEquals(toNumeric(231n), 231n);
  });

  it("[[Call]] converts to a numeric", () => {
    assertStrictEquals(toNumeric("231"), 231);
  });

  it("[[Call]] prefers `valueOf`", () => {
    assertStrictEquals(
      toNumeric({
        toString() {
          return 0;
        },
        valueOf() {
          return 231n;
        },
      }),
      231n,
    );
  });
});

describe("toUintN", () => {
  it("[[Call]] converts to an int·n", () => {
    assertStrictEquals(toUintN(2, 7n), 3n);
  });

  it("[[Call]] works with numbers", () => {
    assertStrictEquals(toUintN(2, 7), 3);
  });

  it("[[Call]] works with non‐integers", () => {
    assertStrictEquals(toUintN(2, 7.21), 3);
    assertStrictEquals(toUintN(2, Infinity), 0);
  });
});
