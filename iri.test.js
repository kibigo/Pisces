// ♓🌟 Piscēs ∷ iri.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  assertEquals,
  assertStrictEquals,
  describe,
  it,
} from "./dev-deps.js";
import {
  composeReference,
  escapeForIRI,
  escapeForURI,
  isAbsoluteIRI,
  isAbsoluteLEIRI,
  isAbsoluteURI,
  isIRI,
  isIRIPath,
  isIRIReference,
  isIRISuffix,
  isLEIRI,
  isLEIRIPath,
  isLEIRIReference,
  isLEIRISuffix,
  isURI,
  isURIPath,
  isURIReference,
  isURISuffix,
  mergePaths,
  parseReference,
  removeDotSegments,
  resolveReference,
} from "./iri.js";

const exampleURIReferences = {
  "ftp://ftp.is.co.za/rfc/rfc1808.txt": {
    scheme: "ftp",
    authority: "ftp.is.co.za",
    path: "/rfc/rfc1808.txt",
  },
  "http://www.ietf.org/rfc/rfc2396.txt": {
    scheme: "http",
    authority: "www.ietf.org",
    path: "/rfc/rfc2396.txt",
  },
  "ldap://[2001:db8::7]/c=GB?objectClass?one": {
    scheme: "ldap",
    authority: "[2001:db8::7]",
    path: "/c=GB",
    query: "objectClass?one",
  },
  "mailto:John.Doe@example.com": {
    scheme: "mailto",
    path: "John.Doe@example.com",
  },
  "news:comp.infosystems.www.servers.unix": {
    scheme: "news",
    path: "comp.infosystems.www.servers.unix",
  },
  "tel:+1-816-555-1212": {
    scheme: "tel",
    path: "+1-816-555-1212",
  },
  "telnet://192.0.2.16:80/": {
    scheme: "telnet",
    authority: "192.0.2.16:80",
    path: "/",
  },
  "urn:oasis:names:specification:docbook:dtd:xml:4.1.2": {
    scheme: "urn",
    path: "oasis:names:specification:docbook:dtd:xml:4.1.2",
  },
  "foo://example.com:8042/over/there?name=ferret#nose": {
    scheme: "foo",
    authority: "example.com:8042",
    path: "/over/there",
    query: "name=ferret",
    fragment: "nose",
  },
  "./this:that": {
    path: "./this:that",
  },
};

// If `path` is non·empty, it must contain an IRI character for tests
// to pass.
const exampleIRIReferences = {
  ...exampleURIReferences,
  "http://ヒキワリ.ナットウ.ニホン": {
    scheme: "http",
    authority: "ヒキワリ.ナットウ.ニホン",
    path: "",
  },
  "http://JP納豆.例.jp/dir1/引き割り.html": {
    scheme: "http",
    authority: "JP納豆.例.jp",
    path: "/dir1/引き割り.html",
  },
  "/dir1/引き割り.html": {
    path: "/dir1/引き割り.html",
  },
};

// If `path` is non·empty, it must contain an LEIRI character for tests
// to pass.
const exampleLEIRIReferences = {
  ...exampleIRIReferences,
  "http://example.com/ foo /": {
    scheme: "http",
    authority: "example.com",
    path: "/ foo /",
  },
  "\0": {
    path: "\0",
  },
};

// These will not parse, so the parse result must be empty.
const exampleReferences = {
  ...exampleLEIRIReferences,
  "\uD800": {},
  "\uFFFE": {},
  "\uFFFF": {},
};

describe("composeReference", () => {
  it("[[Call]] correctly composes references", () => {
    for (
      const [iri, value] of Object.entries(exampleLEIRIReferences)
    ) {
      assertStrictEquals(composeReference(value), iri);
    }
  });
});

describe("escapeForIRI", () => {
  it("[[Call]] converts L·E·I·R·Is to I·R·Is", () => {
    assertStrictEquals(
      escapeForIRI(" æ\0"),
      "%20æ%00",
    );
    assertStrictEquals(
      escapeForIRI("\u{F0000}?\u{F0000}#\u{F0000}"),
      "%F3%B0%80%80?\u{F0000}#%F3%B0%80%80",
    );
  });
});

describe("escapeForURI", () => {
  it("[[Call]] converts L·E·I·R·Is to U·R·Is", () => {
    assertStrictEquals(
      escapeForURI("/dir1/引き割り.html"),
      "/dir1/%E5%BC%95%E3%81%8D%E5%89%B2%E3%82%8A.html",
    );
    assertStrictEquals(
      escapeForURI(" æ\0"),
      "%20%C3%A6%00",
    );
    assertStrictEquals(
      escapeForURI("\u{F0000}?\u{F0000}#\u{F0000}"),
      "%F3%B0%80%80?%F3%B0%80%80#%F3%B0%80%80",
    );
  });
});

describe("isAbsoluteIRI", () => {
  it("[[Call]] identifies absolute I·R·Is", () => {
    for (
      const [iri, { scheme, fragment }] of Object.entries(
        exampleReferences,
      )
    ) {
      assertStrictEquals(
        isAbsoluteIRI(iri),
        iri in exampleIRIReferences && scheme != null &&
          fragment == null,
        iri,
      );
    }
  });
});

describe("isAbsoluteLEIRI", () => {
  it("[[Call]] identifies absolute L·E·I·R·Is", () => {
    for (
      const [leiri, { scheme, fragment }] of Object.entries(
        exampleReferences,
      )
    ) {
      assertStrictEquals(
        isAbsoluteLEIRI(leiri),
        leiri in exampleLEIRIReferences && scheme != null &&
          fragment == null,
        leiri,
      );
    }
  });
});

describe("isAbsoluteURI", () => {
  it("[[Call]] identifies absolute U·R·Is", () => {
    for (
      const [uri, { scheme, fragment }] of Object.entries(
        exampleReferences,
      )
    ) {
      assertStrictEquals(
        isAbsoluteURI(uri),
        uri in exampleURIReferences && scheme != null &&
          fragment == null,
        uri,
      );
    }
  });
});

describe("isIRI", () => {
  it("[[Call]] identifies I·R·Is", () => {
    for (
      const [iri, { scheme }] of Object.entries(exampleReferences)
    ) {
      assertStrictEquals(
        isIRI(iri),
        iri in exampleIRIReferences && scheme != null,
        iri,
      );
    }
  });
});

describe("isIRIPath", () => {
  it("[[Call]] identifies I·R·I paths", () => {
    for (const [iri, { path }] of Object.entries(exampleReferences)) {
      if (path === "") {
        continue;
      } else {
        assertStrictEquals(
          isIRIPath(path ?? iri),
          iri in exampleIRIReferences,
          path,
        );
      }
    }
  });
});

describe("isIRIReference", () => {
  it("[[Call]] identifies I·R·I references", () => {
    for (const iri of Object.keys(exampleReferences)) {
      assertStrictEquals(
        isIRIReference(iri),
        iri in exampleIRIReferences,
        iri,
      );
    }
  });
});

describe("isIRISuffix", () => {
  it("[[Call]] identifies I·R·I suffixes", () => {
    for (
      const [iri, { authority, path }] of Object.entries(
        exampleReferences,
      )
    ) {
      if (!authority) {
        continue;
      } else {
        assertStrictEquals(
          isIRISuffix(authority + path),
          iri in exampleIRIReferences,
          path,
        );
      }
    }
  });
});

describe("isLEIRI", () => {
  it("[[Call]] identifies L·E·I·R·Is", () => {
    for (
      const [leiri, { scheme }] of Object.entries(exampleReferences)
    ) {
      assertStrictEquals(
        isLEIRI(leiri),
        leiri in exampleLEIRIReferences && scheme != null,
        leiri,
      );
    }
  });
});

describe("isLEIRIPath", () => {
  it("[[Call]] identifies L·E·I·R·I paths", () => {
    for (
      const [leiri, { path }] of Object.entries(exampleReferences)
    ) {
      if (path === "") {
        continue;
      } else {
        assertStrictEquals(
          isLEIRIPath(path ?? leiri),
          leiri in exampleLEIRIReferences,
          path,
        );
      }
    }
  });
});

describe("isLEIRIReference", () => {
  it("[[Call]] identifies L·E·I·R·I references", () => {
    for (const leiri of Object.keys(exampleReferences)) {
      assertStrictEquals(
        isLEIRIReference(leiri),
        leiri in exampleLEIRIReferences,
        leiri,
      );
    }
  });
});

describe("isLEIRISuffix", () => {
  it("[[Call]] identifies L·E·I·R·I suffixes", () => {
    for (
      const [leiri, { authority, path }] of Object.entries(
        exampleReferences,
      )
    ) {
      if (!authority) {
        continue;
      } else {
        assertStrictEquals(
          isLEIRISuffix(authority + path),
          leiri in exampleLEIRIReferences,
          path,
        );
      }
    }
  });
});

describe("isURI", () => {
  it("[[Call]] identifies U·R·Is", () => {
    for (
      const [uri, { scheme }] of Object.entries(exampleReferences)
    ) {
      assertStrictEquals(
        isURI(uri),
        uri in exampleURIReferences && scheme != null,
        uri,
      );
    }
  });
});

describe("isURIPath", () => {
  it("[[Call]] identifies U·R·I paths", () => {
    for (const [uri, { path }] of Object.entries(exampleReferences)) {
      if (path === "") {
        continue;
      } else {
        assertStrictEquals(
          isURIPath(path ?? uri),
          uri in exampleURIReferences,
          path,
        );
      }
    }
  });
});

describe("isURIReference", () => {
  it("[[Call]] identifies U·R·I references", () => {
    for (const uri of Object.keys(exampleReferences)) {
      assertStrictEquals(
        isURIReference(uri),
        uri in exampleURIReferences,
        uri,
      );
    }
  });
});

describe("isURISuffix", () => {
  it("[[Call]] identifies U·R·I suffixes", () => {
    for (
      const [uri, { authority, path }] of Object.entries(
        exampleReferences,
      )
    ) {
      if (!authority) {
        continue;
      } else {
        assertStrictEquals(
          isURISuffix(authority + path),
          uri in exampleURIReferences,
          path,
        );
      }
    }
  });
});

describe("mergePaths", () => {
  it("[[Call]] handles the case of an empty base path", () => {
    assertStrictEquals(
      mergePaths("", "etaoin"),
      "/etaoin",
    );
  });

  it("[[Call]] handles the case of a non·empty base path", () => {
    assertStrictEquals(
      mergePaths("/etaoin/cmfwyp", "shrdlu"),
      "/etaoin/shrdlu",
    );
  });
});

describe("parseReference", () => {
  it("[[Call]] correctly parses references", () => {
    for (const [iri, value] of Object.entries(exampleReferences)) {
      assertEquals(parseReference(iri), {
        scheme: undefined,
        authority: undefined,
        path: undefined,
        query: undefined,
        fragment: undefined,
        ...value,
      });
    }
  });
});

describe("removeDotSegments", () => {
  it("[[Call]] correctly removes dot segments", () => {
    assertStrictEquals(removeDotSegments("/a/b/c/./../../g"), "/a/g");
    assertStrictEquals(
      removeDotSegments("mid/content=5/../6"),
      "mid/6",
    );
  });
});

describe("resolveReference", () => {
  it("[[Call]] correctly resolves references", () => {
    const base = "http://a/b/c/d;p?q";
    assertStrictEquals(resolveReference("g:h", base), "g:h");
    assertStrictEquals(resolveReference("g", base), "http://a/b/c/g");
    assertStrictEquals(
      resolveReference("./g", base),
      "http://a/b/c/g",
    );
    assertStrictEquals(
      resolveReference("g/", base),
      "http://a/b/c/g/",
    );
    assertStrictEquals(resolveReference("/g", base), "http://a/g");
    assertStrictEquals(resolveReference("//g", base), "http://g");
    assertStrictEquals(
      resolveReference("?y", base),
      "http://a/b/c/d;p?y",
    );
    assertStrictEquals(
      resolveReference("g?y", base),
      "http://a/b/c/g?y",
    );
    assertStrictEquals(
      resolveReference("#s", base),
      "http://a/b/c/d;p?q#s",
    );
    assertStrictEquals(
      resolveReference("g#s", base),
      "http://a/b/c/g#s",
    );
    assertStrictEquals(
      resolveReference("g?y#s", base),
      "http://a/b/c/g?y#s",
    );
    assertStrictEquals(
      resolveReference(";x", base),
      "http://a/b/c/;x",
    );
    assertStrictEquals(
      resolveReference("g;x", base),
      "http://a/b/c/g;x",
    );
    assertStrictEquals(
      resolveReference("g;x?y#s", base),
      "http://a/b/c/g;x?y#s",
    );
    assertStrictEquals(
      resolveReference("", base),
      "http://a/b/c/d;p?q",
    );
    assertStrictEquals(resolveReference(".", base), "http://a/b/c/");
    assertStrictEquals(resolveReference("./", base), "http://a/b/c/");
    assertStrictEquals(resolveReference("..", base), "http://a/b/");
    assertStrictEquals(resolveReference("../", base), "http://a/b/");
    assertStrictEquals(resolveReference("../g", base), "http://a/b/g");
    assertStrictEquals(resolveReference("../..", base), "http://a/");
    assertStrictEquals(resolveReference("../../", base), "http://a/");
    assertStrictEquals(
      resolveReference("../../g", base),
      "http://a/g",
    );
    assertStrictEquals(
      resolveReference("../../../g", base),
      "http://a/g",
    );
    assertStrictEquals(
      resolveReference("../../../../g", base),
      "http://a/g",
    );
    assertStrictEquals(resolveReference("/./g", base), "http://a/g");
    assertStrictEquals(resolveReference("/../g", base), "http://a/g");
    assertStrictEquals(
      resolveReference("g.", base),
      "http://a/b/c/g.",
    );
    assertStrictEquals(
      resolveReference(".g", base),
      "http://a/b/c/.g",
    );
    assertStrictEquals(
      resolveReference("g..", base),
      "http://a/b/c/g..",
    );
    assertStrictEquals(
      resolveReference("..g", base),
      "http://a/b/c/..g",
    );
    assertStrictEquals(
      resolveReference("./../g", base),
      "http://a/b/g",
    );
    assertStrictEquals(
      resolveReference("./g/.", base),
      "http://a/b/c/g/",
    );
    assertStrictEquals(
      resolveReference("g/./h", base),
      "http://a/b/c/g/h",
    );
    assertStrictEquals(
      resolveReference("g/../h", base),
      "http://a/b/c/h",
    );
    assertStrictEquals(
      resolveReference("g;x=1/./y", base),
      "http://a/b/c/g;x=1/y",
    );
    assertStrictEquals(
      resolveReference("g;x=1/../y", base),
      "http://a/b/c/y",
    );
    assertStrictEquals(
      resolveReference("g?y/./x", base),
      "http://a/b/c/g?y/./x",
    );
    assertStrictEquals(
      resolveReference("g?y/../x", base),
      "http://a/b/c/g?y/../x",
    );
    assertStrictEquals(
      resolveReference("g#s/./x", base),
      "http://a/b/c/g#s/./x",
    );
    assertStrictEquals(
      resolveReference("g#s/../x", base),
      "http://a/b/c/g#s/../x",
    );
    assertStrictEquals(resolveReference("http:g", base), "http:g");
  });
});
