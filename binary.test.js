// ♓🌟 Piscēs ∷ binary.test.js
// ====================================================================
//
// Copyright © 2020–2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  assert,
  assertEquals,
  assertStrictEquals,
  assertThrows,
  describe,
  it,
} from "./dev-deps.js";
import {
  base64Binary,
  base64String,
  filenameSafeBase64Binary,
  filenameSafeBase64String,
  isBase64,
  isFilenameSafeBase64,
} from "./binary.js";

const base64s = {
  "AGIAYQBzAGUANgA0": "base64",
  "R/Q=": new Uint16Array([62535]),
  "S0lCSQ==": new Uint8ClampedArray([75, 73, 66, 73]).buffer,
  YmFzZTY0: new DataView(
    new Uint8Array([98, 97, 115, 101, 54, 52]).buffer,
  ),
};

describe("base64Binary", () => {
  it("[[Call]] returns the correct data", () => {
    assertEquals(
      base64Binary("AGIAYQBzAGUANgA0"),
      new Uint8Array(
        Array.prototype.map.call(
          "\u{0}b\u{0}a\u{0}s\u{0}e\u{0}6\u{0}4",
          ($) => $.charCodeAt(0),
        ),
      ).buffer,
      "AGIAYQBzAGUANgA0",
    );
    assertEquals(
      base64Binary("R/Q="),
      new Uint16Array([62535]).buffer,
      "R/Q=",
    );
    assertEquals(
      base64Binary("S0lCSQ=="),
      new Uint8ClampedArray([75, 73, 66, 73]).buffer,
      "S0lCSQ==",
    );
    assertEquals(
      base64Binary("YmFzZTY0"),
      new Uint8Array([98, 97, 115, 101, 54, 52]).buffer,
      "YmFzZTY0",
    );
  });

  it("[[Call]] throws when provided with an invalid character", () => {
    assertThrows(() => base64Binary("abc_"));
  });

  it("[[Call]] throws when provided with an invalid equals", () => {
    assertThrows(() => base64Binary("abc=="));
  });

  it("[[Call]] throws when provided with a length with a remainder of 1 when divided by 4", () => {
    assertThrows(() => base64Binary("abcde"));
    assertThrows(() => base64Binary("abcde==="));
  });
});

describe("base64String", () => {
  it("[[Call]] returns the correct string", () => {
    Object.entries(base64s).forEach(([key, value]) =>
      assertStrictEquals(base64String(value), key)
    );
  });
});

describe("filenameSafeBase64Binary", () => {
  it("[[Call]] returns the correct data", () => {
    assertEquals(
      filenameSafeBase64Binary("AGIAYQBzAGUANgA0"),
      new Uint8Array(
        Array.prototype.map.call(
          "\u{0}b\u{0}a\u{0}s\u{0}e\u{0}6\u{0}4",
          ($) => $.charCodeAt(0),
        ),
      ).buffer,
      "AGIAYQBzAGUANgA0",
    );
    assertEquals(
      filenameSafeBase64Binary("R_Q="),
      new Uint16Array([62535]).buffer,
      "R/Q=",
    );
    assertEquals(
      filenameSafeBase64Binary("S0lCSQ=="),
      new Uint8ClampedArray([75, 73, 66, 73]).buffer,
      "S0lCSQ==",
    );
    assertEquals(
      filenameSafeBase64Binary("YmFzZTY0"),
      new Uint8Array([98, 97, 115, 101, 54, 52]).buffer,
      "YmFzZTY0",
    );
  });

  it("[[Call]] throws when provided with an invalid character", () => {
    assertThrows(() => filenameSafeBase64Binary("abc/"));
  });

  it("[[Call]] throws when provided with an invalid equals", () => {
    assertThrows(() => filenameSafeBase64Binary("abc=="));
  });

  it("[[Call]] throws when provided with a length with a remainder of 1 when divided by 4", () => {
    assertThrows(() => filenameSafeBase64Binary("abcde"));
    assertThrows(() => filenameSafeBase64Binary("abcde==="));
  });
});

describe("filenameSafeBase64String", () => {
  it("[[Call]] returns the correct string", () => {
    Object.entries(base64s).forEach(([key, value]) =>
      assertStrictEquals(
        filenameSafeBase64String(value),
        key.replace("+", "-").replace("/", "_"),
      )
    );
  });
});

describe("isBase64", () => {
  it("[[Call]] returns true for base64 strings", () => {
    Object.keys(base64s).forEach((key) => assert(isBase64(key)));
  });

  it("[[Call]] returns false for others", () => {
    [
      undefined,
      null,
      true,
      Symbol(),
      27,
      98n,
      {},
      [],
      () => {},
      new Proxy({}, {}),
      "abc_",
      "a",
      "abc==",
    ].forEach((value) => assert(!isBase64(value)));
  });
});

describe("isFilenameSafeBase64", () => {
  it("[[Call]] returns true for filename‐safe base64 strings", () => {
    Object.keys(base64s).forEach((key) =>
      assert(
        isFilenameSafeBase64(key.replace("+", "-").replace("/", "_")),
      )
    );
  });

  it("[[Call]] returns false for others", () => {
    [
      undefined,
      null,
      true,
      Symbol(),
      27,
      98n,
      {},
      [],
      () => {},
      new Proxy({}, {}),
      "abc/",
      "a",
      "abc==",
    ].forEach((value) => assert(!isFilenameSafeBase64(value)));
  });
});
