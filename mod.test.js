// ♓🌟 Piscēs ∷ mod.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { assert, describe, it } from "./dev-deps.js";
import * as Piscēs from "./mod.js";

describe("Piscēs", () => {
  it("exports everything", async () => {
    for await (const { name, isFile } of Deno.readDir(".")) {
      if (isFile && /(?<!^mod|\.test|(?:^|-)deps)\.js$/u.test(name)) {
        await import(`./${name}`).then((module) => {
          for (const exported of Object.keys(module)) {
            assert(exported in Piscēs);
          }
        });
      }
    }
  });
});
