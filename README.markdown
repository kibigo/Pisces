# ♓🧩 Piscēs

<b>Ecmascript building blocks.</b>

<dfn>♓🧩 Piscēs</dfn> is a Javascript (handcoded Ecmascript) library
providing various building blocks for developing complex,
precisely‐defined Javascript libraries. It provides implementations of
fundamental Ecmascript Specification functions for use in Javascript
code, plus other helpful classes and utilities. It strives to have a no
runtime dependencies on the Ecmascript default bindings (loadtime
dependencies are, at present, unavoidable).

**♓🧩 Piscēs was written to serve the interests of [♓🌟 الرشآء][Alrescha]
development, and not as a general‐use library.** This means that it is
not extensively documented beyond its source code, it is not rigidly
versioned, and it offers no guarantees regarding stability between
versions. However, it is written in a generic manner, such that if you
find it useful, you _can_ integrate it into your project—using ordinary
Ecmascript imports from
`https://gitlab.com/kibigo/Pisces/-/raw/❲HASH❳/mod.js`. Always use a
specific hash (or tag), and not a branch name like `current`, when
importing ♓🧩 Piscēs into your project.

> **Note:** At time of writing, GitLab lacks C·O·R·S support for raw
> files in public repositories (see [this issue][GitLab-CORS]). This is
> not a problem for Deno, but may prove an issue for browsers.

Piscēs is broken up into multiple files, which is perfectly acceptable
for use with [Deno][Deno], but generally not considered ideal for usage
on the web. You can use the `deno bundle` command to stitch the various
source files together into a single script.

## License

Source files are licensed under the terms of the <cite>Mozilla Public
License, version 2.0</cite>. For more information, see
[LICENSE](./LICENSE).

[Alrescha]: https://gitlab.com/kibigo/Alrescha
[Deno]: https://deno.land
[GitLab-CORS]: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/24596
